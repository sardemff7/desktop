# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Copyright 2015 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MOZ_CODENAME="Daily"

linguas=(
    af ar ast be bg br ca cak cs cy da de dsb el en_CA en_GB en_US es_AR es_ES et eu fa fi fr fy_NL
    ga_IE gd gl he hr hsb hu hy_AM id is it ja ka kab kk ko lt ms nb_NO nl nn_NO pa_IN pl pt_BR
    pt_PT rm ro ru si sk sl sq sr sv_SE th tr uk uz vi zh_CN zh_TW
)

require mozilla-pgo [ co_project=comm/mail codename="${MOZ_CODENAME}" ]
# FIXME: The bindist option as handled by mozilla.exlib doesn't work with current
# Thunderbirds anymore. I couldn't find out how to do it these days either so for
# now it's gone.
require toolchain-funcs utf8-locale

SUMMARY="Mozilla's standalone mail and news client"
HOMEPAGE="https://www.mozilla.com/en-US/${PN}/"
DOWNLOADS="https://ftp.mozilla.org/pub/${PN}/releases/${PV}/source/${PNV}.source.tar.xz"
for lang in "${linguas[@]}" ; do
    DOWNLOADS+="
        linguas:${lang}? ( https://ftp.mozilla.org/pub/${PN}/releases/${PV}/linux-x86_64/xpi/${lang/_/-}.xpi -> ${PNV}-${lang}.xpi )
    "
done

REMOTE_IDS="freshcode:${PN}"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}${PV}/releasenotes/"

LICENCES="MPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    accessibility
    libproxy    [[ description = [ Use libproxy for system proxy settings ] ]]
    necko-wifi  [[ description = [ Scan WiFi with its internal network library ] ]]
    pulseaudio
    wayland   [[ description = [ Use wayland as the default backend ] ]]

    ( linguas: ${linguas[@]} )
    ( providers: jpeg-turbo )
    ( providers: botan openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-arch/zip
        dev-lang/nasm
        dev-lang/clang:*[>=9.0]
        dev-lang/perl:*[>=5.0]
        dev-lang/python:*[>=3.6][sqlite]
        dev-python/ply
        dev-rust/cbindgen[>=0.26.0]
        virtual/pkg-config
        virtual/unzip
    build+run:
        app-spell/hunspell:=
        dev-lang/llvm:=[>=8.0]
        dev-lang/node
        dev-lang/rust:*[>=1.51]
        dev-libs/glib:2[>=2.42]
        dev-libs/icu:=[>=73.1]
        dev-libs/libevent:=
        dev-libs/libffi:=[>=3.0.9]
        dev-libs/nspr[>=4.32]
        media-libs/fontconfig[>=2.7.0]
        media-libs/freetype:2[>=2.2.0] [[ note = [ aka 9.10.3 ] ]]
        media-libs/libvpx:=[>=1.10.0]
        media-libs/libwebp:=[>=1.0.2]
        sys-apps/dbus[>=0.60]
        sys-libs/zlib[>=1.2.3]
        sys-sound/alsa-lib
        x11-dri/libdrm[>=2.4]
        x11-libs/cairo[X][>=1.10]
        x11-libs/gdk-pixbuf:2.0[>=2.26.5]
        x11-libs/gtk+:3[>=3.22.0][wayland]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXext
        x11-libs/libxkbcommon[>=0.4.1]
        x11-libs/libXrandr[>=1.4.0]
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXt
        x11-libs/pango[>=1.22][X(+)]
        x11-libs/pixman:1[>=0.36.0]
        libproxy? ( net-libs/libproxy:1 )
        necko-wifi? ( net-wireless/wireless_tools )
        !pgo? ( dev-libs/nss[>=3.99] )
        providers:botan? ( dev-libs/botan:=[>=2.8.0] )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.1e] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        pulseaudio? ( media-sound/pulseaudio )
    suggestion:
        x11-libs/libnotify[>=0.4] [[ description = [ Show notifications with libnotify ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-fix-rnp-openssl.patch
    "${FILES}"/05cb55554fc0.patch
)

MOZILLA_SRC_CONFIGURE_PARAMS=(
    --disable-install-strip
    --disable-strip
    # webrtc fails to build, missing pk11pub.h
    --disable-webrtc
    # HTML5 media support
    --enable-alsa
    --enable-dbus
    --enable-release
    --enable-rust-simd
    --enable-webspeech

    --enable-system-pixman
    --with-system-ffi
    --with-system-icu
    --with-system-libevent
    --with-system-libvpx
    #--with-system-png # needs libpng with APNG
    --with-system-webp
    --with-system-zlib
    # Probably a good idea to enable this at some point, but we would need
    # wasi-sdk (https://github.com/WebAssembly/wasi-sdk)
    --without-wasm-sandboxed-libraries

    # v requires sqlite built with -DSQLITE_SECURE_DELETE
    # v and enabling it resulted in linker issues
    #--enable-system-sqlite

    # optimizations
    --disable-crashreporter
    --disable-simulator
    --disable-updater

    --enable-hardening
    --enable-sandbox
)

MOZILLA_SRC_CONFIGURE_OPTIONS=(
    # pgo fails to build with system nss: "x86_64-pc-linux-gnu-ld: error:
    # cannot find -lnssutil3"
    'debug --without-system-nss'
    'pgo --without-system-nss'
)

MOZILLA_SRC_CONFIGURE_OPTION_ENABLES=(
    accessibility
    libproxy
    necko-wifi
)
MOZILLA_SRC_CONFIGURE_OPTION_WITHS=(
    # --with-system-jpeg needs libjpeg-turbo or whatever provides JCS_EXTENSIONS
    'providers:jpeg-turbo system-jpeg'
)

pkg_pretend() {
    if has_version ${CATEGORY}/${PN}[bindist]; then
        ewarn "The bindist option had to be hard-disabled for ${CATEGORY}/${PNV}. Your current installation"
        ewarn "has this option enabled. You should be aware that you must not distribute builds"
        ewarn "without the bindist option. If this is a problem for you, fix the exheres."
    fi
}

src_unpack() {
    default

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            edo mkdir "${WORKBASE}"/${lang}
            edo unzip -qo "${FETCHEDDIR}"/${PNV}-${lang}.xpi -d "${WORKBASE}"/${lang}
        fi
    done
}

src_prepare() {
    require_utf8_locale

    export TOOLCHAIN_PREFIX=$(exhost --tool-prefix)

    # those tests seem to fail in non-UTC timezone
    edo rm js/src/jit-test/tests/sunspider/check-date-format-{xparb,tofte}.js

    mozilla-pgo_src_prepare

    edo sed -i -e "s:objdump:${TOOLCHAIN_PREFIX}&:" python/mozbuild/mozbuild/configure/check_debug_ranges.py

    has_version "dev-libs/icu:74.1" && \
        expatch "${FILES}"/d5f3b0c4f08a426ce00a153c04e177eecb6820e2.patch

    edo mkdir "${TEMP}/mozbuild"
}

src_configure() {
    # x86_64-pc-linux-gnu-as: invalid option -- 'N'
    export AS=$(exhost --tool-prefix)cc

    if cxx-is-gcc; then
        botan_compiler=gcc
    elif cxx-is-clang; then
        botan_compiler=clang
    else
        die "Unknown compiler ${CXX}; you will need to add a check for it to the thunderbird exheres"
    fi

    local audio_backends="alsa"
    option pulseaudio && audio_backends+=",pulseaudio"

    local params=(
        EXHERBO_BOTAN_CC=${botan_compiler} \
        $(cc-is-clang || echo "--with-clang-path=/usr/$(exhost --build)/bin/$(exhost --tool-prefix)clang") \
        $(echo "--disable-elf-hack") \
        --enable-audio-backends=${audio_backends} \
        --enable-default-toolkit=cairo-gtk3$(option wayland -wayland)
    )

    if option providers:botan ; then
        params+=(
            --with-librnp-backend=botan
            --with-system-botan
        )
    else
        # probably works with libressl too, but I have not tested that
        params+=( --with-librnp-backend=openssl )
    fi

    export MOZBUILD_STATE_PATH="${TEMP}/mozbuild"

    esandbox allow_net --connect "unix:/run/uuidd/request"
    mozilla-pgo_src_configure "${params[@]}"
    esandbox disallow_net --connect "unix:/run/uuidd/request"
}

src_install() {
    mozilla-pgo_src_install

    # allow installation of distributed extensions and read/use system locale on runtime
    insinto /usr/$(exhost --target)/lib/${PN}/defaults/pref
    hereins all-exherbo.js <<EOF
pref("extensions.autoDisableScopes", 3);
pref("intl.locale.requested", "");
EOF

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            # Extract an id from the localisation tarball, usually something
            # like langpack-${lang}@thunderbird.mozilla.org. Not entirely sure
            # which rules it follows so I go with cargo-cult for now.
            emid="$(sed -n -e 's/.*"id": "\(.*\)",/\1/p' "${WORKBASE}"/${lang}/manifest.json | xargs)"
            insinto /usr/$(exhost --target)/lib/${PN}/distribution/extensions
            newins "${FETCHEDDIR}"/${PNV}-${lang}.xpi ${emid}.xpi
        fi
    done
}

