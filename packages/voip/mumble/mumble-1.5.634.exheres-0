# Copyright 2010-2011 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Copyright 2012-2013 Lasse Brun <bruners@gmail.com>
# Copyright 2013-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mumble-1.2.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2010 Gentoo Foundation

require github [ user=${PN}-voip release=v${PV} suffix=tar.gz ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache \
    option-renames [ renames=[ 'speechd tts' ] ]

SUMMARY="Mumble is an open source, low-latency, high quality voice chat software"
DESCRIPTION="
Mumble is a voice chat application for groups. While it can be used for any kind of activity, it is
primarily intended for gaming. It can be compared to programs like Ventrilo or TeamSpeak. People
tend to simplify things, so when they talk about Mumble they either talk about \"Mumble\" the
client application or about \"Mumble & Mumble Server\" the whole voice chat application suite.
"
HOMEPAGE+=" https://${PN}.info"

LICENCES="BSD-3 MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    alsa
    avahi
    jack
    pipewire
    pulseaudio
    tts [[ description = [ Support for text to speech ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-libs/json
        virtual/pkg-config
        x11-libs/qttools:5 [[ note = [ Qt5LinguistTools ] ]]
        x11-proto/xorgproto
    build+run:
        dev-libs/boost
        dev-libs/poco
        dev-libs/protobuf:=
        media-libs/libsndfile
        media-libs/opus[>=1.2.1]
        media-libs/speex[>=1.2_rc1]
        media-libs/speexdsp[>=1.2_rc1]
        x11-libs/libX11
        x11-libs/libXi
        x11-libs/qtbase:5[gui][sql][sqlite]
        x11-libs/qtsvg:5
        alsa? ( sys-sound/alsa-lib )
        avahi? ( net-dns/avahi[dns_sd] )
        jack? ( media-sound/jack-audio-connection-kit )
        pipewire? ( media/pipewire )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        pulseaudio? ( media-sound/pulseaudio )
        tts? ( app-speech/speechd )
        !media-sound/mumble [[
            description = [ media-sound/mumble was moved to ::desktop voip/mumble ]
            resolution = uninstall-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_CXX_STANDARD:STRING=17
    -DBUILD_NUMBER:STRING=$(ever range 3)
    -Dbenchmarks:BOOL=FALSE
    -Dbundle-qt-translations:BOOL=FALSE
    -Dbundled-gsl:BOOL=TRUE
    -Dbundled-json:BOOL=FALSE
    -Dbundled-renamenoise:BOOL=TRUE
    -Dbundled-speex:BOOL=FALSE
    -Dclient:BOOL=TRUE
    -Ddebug-dependency-search:BOOL=FALSE
    -Ddisplay-install-paths:BOOL=TRUE
    -Dg15:BOOL=FALSE
    -Dlto:BOOL=FALSE
    -Dmanual-plugin:BOOL=TRUE
    -Doptimize:BOOL=FALSE
    -Doverlay:BOOL=FALSE
    -Doverlay-xcompile:BOOL=FALSE
    -Dpackaging:BOOL=FALSE
    -Dplugin-callback-debug:BOOL=FALSE
    -Dplugin-debug:BOOL=FALSE
    -Dplugins:BOOL=FALSE
    -Dportaudio:BOOL=FALSE
    -Dqssldiffiehellmanparameters:BOOL=TRUE
    -Dqtspeech:BOOL=FALSE
    -Dretracted-plugins:BOOL=FALSE
    -Dserver:BOOL=FALSE
    -Dstatic:BOOL=FALSE
    -Dsymbols:BOOL=FALSE
    -Dtests:BOOL=FALSE
    -Dtracy:BOOL=FALSE
    -Dtranslations:BOOL=TRUE
    -Dupdate:BOOL=FALSE
    -Dwarnings-as-errors:BOOL=FALSE
    -Dxinput2:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    alsa
    'avahi zeroconf'
    'jack jackaudio'
    pipewire
    pulseaudio
    'tts speechd'
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst

    elog "Mumble supports reading the kernel input devices, but may fall back to using the less optimal xinput2"
    elog "This can be solved with a simple udev rule: SUBSYSTEM==\"input\", GROUP=\"input\" MODE=\"660\""
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

