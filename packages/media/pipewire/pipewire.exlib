# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" suffix=tar.bz2 new_download_scheme=true ] \
    meson ffmpeg [ with_opt=true ] systemd-service udev-rules

export_exlib_phases pkg_setup src_prepare src_test src_install pkg_postinst

SUMMARY="PipeWire is a project that aims to greatly improve handling of audio and video under Linux"

HOMEPAGE+=" https://pipewire.org/"

LICENCES="
    GPL-2 [[ note = [ pipewire-jack/jack/control.h ] ]]
    LGPL-2.1 [[ note = [ spa/plugins/alsa ] ]]
    MIT
"
SLOT="0"

MYOPTIONS="
    alsa [[ description = [ Build Pipewire's ALSA plugin and provide a default output ] ]]
    avahi
    bluetooth [[ description = [ Build Pipewire's BlueZ plugin ] ]]
    camera [[ description = [ Support camera devices using libcamera ] ]]
    dbus
    doc
    echo-cancel-webrtc [[ description = [ Build Pipewire's WebRTC-based echo canceller module ] ]]
    ffmpeg [[ description = [ Build Pipewire's FFmpeg plugin and pw-cat compress offload support ] ]]
    gstreamer [[ description = [ Build Pipewire's GStreamer plugins ] ]]
    jack [[ description = [ Build Pipewire's JACK plugin ] ]]
    opus [[ description = [ Build code that depends on opus ] ]]
    pulseaudio [[ description = [ Module to tunnel audio to/from a (remote) PulseAudio server ] ]]
    raop [[ description = [ Remote Audio Output Protocol (Airplay 2) ] ]]
    systemd
    X [[ description = [ Module to listen for X11 bell events and playing a sample ] ]]
    (
        aptx [[ description = [ Support the Bluetooth aptX codec ] ]]
        fdk-aac [[ description = [ Support the Bluetooth AAC codec ] ]]
        ldac [[ description = [ Support the Bluetooth LDAC codec ] ]]
    ) [[ *requires = [ bluetooth ] ]]
    raop [[ requires = [ avahi opus ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/doxygen[>=1.9]
        dev-lang/python:*[>=3]
        sys-devel/gettext
        sys-libs/vulkan-headers[>=1.2.170]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32.0]
        media-libs/libsndfile[>=1.0.20]
        sys-libs/ncurses
        sys-libs/readline:=
        sys-libs/vulkan-loader[>=1.1.69]
        sys-sound/alsa-lib[>=1.1.7]
        x11-dri/libdrm
        aptx? ( media-libs/libfreeaptx )
        avahi? ( net-dns/avahi[dbus] )
        bluetooth? (
            dev-libs/libusb:1
            media-libs/sbc
            net-wireless/bluez
        )
        camera? ( media-libs/libcamera[>=0.2.0] )
        dbus? ( sys-apps/dbus )
        echo-cancel-webrtc? ( media-libs/webrtc-audio-processing:1[>=1.2] )
        fdk-aac? ( media-libs/fdk-aac )
        gstreamer? (
            media-libs/gstreamer:1.0[>=1.10.0]
            media-plugins/gst-plugins-base:1.0[>=1.23.1]
        )
        jack? ( media-sound/jack-audio-connection-kit[>=1.9.10] )
        ldac? ( media-libs/libldac )
        opus? ( media-libs/opus )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? ( media-sound/pulseaudio )
        raop? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd )
        X? (
            media-libs/libcanberra
            x11-libs/libX11
            x11-libs/libXfixes
        )
    post:
        media/wireplumber
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dalsa=enabled
    -Daudioconvert=enabled
    -Daudiomixer=enabled
    -Daudiotestsrc=enabled
    -Davb=enabled
    -Dbluez5-backend-native-mm=disabled
    -Dbluez5-codec-lc3=disabled
    -Dbluez5-codec-lc3plus=disabled
    -Dbluez5-codec-opus=disabled
    -Dcontrol=enabled
    -Ddoc-prefix-value=/usr
    -Ddoc-sysconfdir-value=/etc
    -Ddocdir=/usr/share/doc/${PNVR}
    -Devl=disabled
    -Dexamples=disabled
    -Dflatpak=enabled
    -Dgsettings=enabled
    -Dgsettings-pulse-schema=disabled
    -Dinstalled_tests=disabled
    -Djack-devel=false
    -Dlegacy-rtkit=false
    -Dlibffado=disabled
    -Dlibmysofa=disabled
    -Dlv2=disabled
    -Dman=enabled
    -Dpam-defaults-install=false
    -Dpipewire-v4l2=enabled
    -Dpw-cat=enabled
    -Dreadline=enabled
    -Drlimits-install=true
    -Drlimits-match=@users
    -Droc=disabled
    -Drtprio-client=83
    -Drtprio-server=88
    -Dsdl2=disabled
    -Dselinux=disabled
    -Dsession-managers=/usr/$(exhost --target)/bin/wireplumber
    -Dsnap=disabled
    -Dsndfile=enabled
    -Dspa-plugins=enabled
    -Dsupport=enabled
    -Dsystemd-system-unit-dir=${SYSTEMDSYSTEMUNITDIR}
    -Dsystemd-user-unit-dir=${SYSTEMDUSERUNITDIR}
    -Dtest=enabled
    -Dudev=enabled
    -Dudevrulesdir=${UDEVRULESDIR}
    -Dv4l2=enabled
    -Dvideoconvert=enabled
    -Dvideotestsrc=enabled
    -Dvolume=disabled
    -Dvulkan=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'alsa compress-offload'
    'alsa pipewire-alsa'
    'aptx bluez5-codec-aptx'
    avahi
    'bluetooth bluez5'
    'bluetooth bluez5-backend-hfp-native'
    'bluetooth bluez5-backend-hsphfpd'
    'bluetooth bluez5-backend-hsp-native'
    'bluetooth bluez5-backend-ofono'
    'bluetooth libusb'
    'camera libcamera'
    dbus
    'doc docs'
    echo-cancel-webrtc
    'fdk-aac bluez5-codec-aac'
    ffmpeg
    'ffmpeg pw-cat-ffmpeg'
    gstreamer
    'gstreamer gstreamer-device-provider'
    jack
    'jack pipewire-jack'
    'ldac bluez5-codec-ldac'
    opus
    'pulseaudio libpulse'
    'raop'
    systemd
    'systemd systemd-system-service'
    'systemd systemd-user-service'
    'X libcanberra'
    'X x11'
    'X x11-xfixes'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

pipewire_pkg_setup() {
    meson_pkg_setup
    ffmpeg_pkg_setup
}

pipewire_src_prepare() {
    meson_src_prepare

    # remove failing test: 'core != NULL' failed, last checked: 1.2.0
    edo sed \
        -e '/test-security-context/d' \
        -i src/tests/meson.build
}

pipewire_src_test() {
    esandbox allow '/memfd:pipewire-memfd:*'
    meson_src_test
}

pipewire_src_install() {
    meson_src_install

    if option alsa ; then
        # see ALSA plugin in INSTALL.md
        dodir /etc/alsa/conf.d
        edo cp "${IMAGE}"/usr/share/alsa/alsa.conf.d/50-pipewire.conf "${IMAGE}"/etc/alsa/conf.d/50-pipewire.conf
        edo cp "${IMAGE}"/usr/share/alsa/alsa.conf.d/99-pipewire-default.conf "${IMAGE}"/etc/alsa/conf.d/99-pipewire-default.conf
    fi
}

pipewire_pkg_postinst() {
    local cruft=( /etc/security/limits.d/95-pipewire.conf )
    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || ewarn "removing ${file} failed"
        fi
    done
}

