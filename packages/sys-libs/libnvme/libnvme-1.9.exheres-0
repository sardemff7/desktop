# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=linux-nvme tag=v${PV} ] meson

SUMMARY="C Library for NVM Express on Linux"
DESCRIPTION="
It provides type definitions for NVMe specification structures, enumerations,
and bit fields, helper functions to construct, dispatch, and decode commands
and payloads, and utilities to connect, scan, and manage nvme devices on a
Linux system."

LICENCES="
 || ( LGPL-2.1 LGPL-3 )
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    tls [[ description = [ Support for TLS with NVMe over TCP ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/json-c:=[>=0.13]
        sys-apps/dbus
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
        tls? ( sys-apps/keyutils )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddocs=man
    # Support for /etc/nvme/config.json
    -Djson-c=enabled
    -Dlibdbus=enabled
    -Dopenssl=enabled
    -Dpython=disabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'tls keyutils'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

