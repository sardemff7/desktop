# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=storaged-project release=${PNV} suffix=tar.bz2 ] \
    systemd-service \
    udev-rules \
    bash-completion zsh-completion

SUMMARY="Storage daemon that implements well-defined D-Bus interfaces"
HOMEPAGE+=" https://www.freedesktop.org/wiki/Software/${PN}"

LICENCES="
    GPL-2 [[ note = [ daemon and tools ] ]]
    LGPL-2.1 [[ note = [ libraries ] ]]
"
SLOT="2"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    btrfs [[ description = [ Support for Btrfs filesystems ] ]]
    gobject-introspection
    gtk-doc
    lvm [[ description = [ Support for LVM volumes ] ]]
    ( linguas: af ar as az bg bn_IN ca ca@valencia cs cy da de el en_GB eo es et eu fa fi fo fr fur
               ga gl gu he hi hr hu ia id it ja ka kk kn ko lt lv ml mr ms nb nl nn oc or pa pl pt
               pt_BR ro ru sk sl sq sr sr@latin sv ta te th tr uk vi wa zh_CN zh_HK zh_TW )
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: elogind systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext[>=0.19.8]
        sys-kernel/linux-headers[>=3.1] [[ note = [ for LOOP_CTL_GET_FREE in loop.h ] ]]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.3] )
    build+run:
        base/libatasmart[>=0.17]
        base/libblockdev[>=3.0][cryptsetup][mdraid][nvme]
        dev-libs/glib:2[>=2.68]
        gnome-desktop/libgudev[>=209]
        sys-apps/acl
        sys-apps/util-linux[>=2.30]
        sys-auth/polkit:1[>=0.102]
        btrfs? ( base/libblockdev[btrfs] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.2] )
        lvm? ( base/libblockdev[lvm] )
        providers:elogind? ( sys-auth/elogind[>=219] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=209] )
    suggestion:
        sys-fs/dosfstools [[ description = [ Support for FAT16/32 filesystems ] ]]
        sys-fs/ntfs-3g_ntfsprogs [[ description = [ Support for NTFS filesystems ] ]]
        sys-fs/udftools [[ description = [ Support for UDF filesystems ] ]]
        sys-fs/xfsprogs [[ description = [ Support for XFS filesystems ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-acl
    --enable-daemon
    --enable-man
    --enable-nls
    --disable-fhs-media
    --disable-iscsi
    --disable-lsm
    --disable-static
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-udevdir="${UDEVDIR}"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    btrfs
    "gobject-introspection introspection"
    gtk-doc
    "lvm lvm2"
)

DEFAULT_SRC_TEST_PARAMS=( TESTS_ENVIRONMENT= )

# test_spawned_job_failure asserts under sydbox
RESTRICT="test"

src_install() {
    default

    edo rm "${IMAGE}"/usr/share/bash-completion/completions/udisksctl
    edo rmdir "${IMAGE}"/usr/share/bash-completion/{completions,}
    dobashcompletion "${WORK}"/data/completions/udisksctl
    option zsh-completion || edo rm -r "${IMAGE}"/usr/share/zsh

    keepdir /etc/udisks2
    keepdir /var/lib/udisks2

    # avoid leaking information about e.g. mounts to unprivileged users
    edo chmod 0700 "${IMAGE}"/var/lib/udisks2
}

pkg_postinst() {
    nonfatal edo udevadm trigger --subsystem-match=block --action=change
}

pkg_postrm() {
    nonfatal edo udevadm trigger --subsystem-match=block --action=change
}

