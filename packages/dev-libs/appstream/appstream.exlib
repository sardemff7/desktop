# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ximion ]
require meson test-dbus-daemon
require alternatives

if ever at_least 1.0.0 ; then
    require option-renames [ renames=[ 'qt6 providers:qt6' ] ]
fi

export_exlib_phases src_prepare src_configure src_test src_install

SUMMARY="Tools and libraries to work with AppStream XML/YAML metadata"
DESCRIPTION="
AppStream is a cross-distribution effort for enhancing the way we interact with
the software repositories provided by (Linux) distributions by standardizing
software component metadata.

It provides the foundation to build software-center applications, by providing
metadata necessary for an application-centric view on package repositories.
AppStream additionally provides specifications for things like an unified
software metadata database, screenshot services and various other things needed
to create user-friendly application-centers for (Linux) distributions."

HOMEPAGE+=" https://www.freedesktop.org/wiki/Distributions/AppStream/"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/releases/AppStream-${PV}.tar.xz"

UPSTREAM_DOCUMENTATION="https://www.freedesktop.org/software/${PN}/docs/"

LICENCES="LGPL-2.1"
MYOPTIONS="
    compose [[ description = [ Experimental lib for composing AppStream metadata ] ]]
    doc
    gobject-introspection
    systemd
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        dev-util/gperf
        dev-util/itstool
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.62]
        dev-libs/libxml2:2.0
        dev-libs/libyaml
        net-misc/curl[>=7.62]
        compose? (
            gnome-desktop/librsvg:2[>=2.48]
            media-libs/fontconfig
            media-libs/freetype:2
            x11-libs/cairo[>=1.12]
            x11-libs/gdk-pixbuf:2.0
            x11-libs/pango
        )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.56] )
        systemd? ( sys-apps/systemd )
    run:
        !dev-libs/appstream:0[<0.16.3-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 1.0.1 ; then
    MYOPTIONS+="
        ( providers: qt5 qt6 )
        doc [[ requires = gobject-introspection ]]
    "

    DEPENDENCIES+="
        build:
            app-text/docbook-xsl-stylesheets
            doc? ( dev-doc/gi-docgen[>=2021.1] )
        build+run:
            app-arch/zstd
            dev-libs/libxmlb[>=0.3.14]
            providers:qt5? ( x11-libs/qtbase:5[>=5.15] )
            providers:qt6? ( x11-libs/qtbase:6[>=6.2.4] )
        test:
            providers:qt5? ( x11-libs/qtbase:5[>=5.15][glib] )
            providers:qt6? ( x11-libs/qtbase:6[>=6.2.4][glib] )
    "

else
    MYOPTIONS+="
        qt5
    "

    DEPENDENCIES+="
        build:
            app-text/docbook-xml-dtd:4.5
            doc? ( dev-doc/gtk-doc )
        build+run:
            dev-libs/libxmlb[>=0.3.13]
            qt5? ( x11-libs/qtbase:5 )
    "
fi

MESON_SOURCE="${WORKBASE}/AppStream-${PV}"

appstream_src_prepare() {
    meson_src_prepare

    # Fixes paths in the cmake config file. TODO: Figure out how to fix this
    # properly upstream.
    edo sed \
        -e "/get_filename_component(PACKAGE_PREFIX_DIR/s/\/..//" \
        -i qt/cmake/AppStreamQtConfig.cmake.in

    # Fix wrong docdir (PN isntead of PNVR)
    edo sed \
        -e "/as_doc_target_dir/ s/'appstream'/'${PNVR}'/" \
        -i docs/meson.build
}

appstream_src_configure() {
    local meson_params+=(
        -Dapt-support=false
        # Needs python3 and https://github.com/openSUSE/daps, the latter unwritten
        -Ddocs=false
        -Dstatic-analysis=false
        # Needs http://snowball.tartarus.org/dist/libstemmer_c.tgz/
        # https://github.com/zvelo/libstemmer - unreleased and unwritten
        -Dstemming=false
        # Feel free to make this optional if you need it.
        -Dvapi=false

        $(meson_switch compose)
        $(meson_switch compose svg-support)
        $(meson_switch doc apidocs)
        $(meson_switch gobject-introspection gir)
        $(meson_switch systemd)
    )

    if ever at_least 1.0.1 ; then
        meson_params+=(
            -Dzstd-support=true
        )


        if option providers:qt5 && option providers:qt6 ; then
            meson_params+=(
                "-Dqt-versions=[ '5', '6' ]"
                -Dqt=true
            )
        else
            if option providers:qt5 ; then
                meson_params+=(
                    "-Dqt-versions=[ '5' ]"
                    -Dqt=true
                )
            elif option providers:qt6 ; then
                meson_params+=(
                    "-Dqt-versions=[ '6' ]"
                    -Dqt=true
                )
            else
                meson_params+=( -Dqt=false )
            fi
        fi
    else
        meson_params+=(
            $(meson_switch qt5 qt)
        )
    fi

    exmeson "${meson_params[@]}"
}

appstream_src_test() {
    test-dbus-daemon_start
    meson_src_test
    test-dbus-daemon_stop
}

appstream_src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    meson_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/bin/appstreamcli       appstreamcli-${SLOT}
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    if ever at_least 1.0.0 ; then
        if option providers:qt5 ; then
            arch_dependent_alternatives+=(
                /usr/${host}/include/AppStreamQt5   AppStreamQt5-${SLOT}
                /usr/${host}/lib/libAppStreamQt5.so libAppStreamQt5-${SLOT}.so
                /usr/${host}/lib/cmake/AppStreamQt5 AppStreamQt5-${SLOT}
            )
        fi

        if option providers:qt6 ; then
            arch_dependent_alternatives+=(
                /usr/${host}/include/AppStreamQt    AppStreamQt-${SLOT}
                /usr/${host}/lib/libAppStreamQt.so  libAppStreamQt-${SLOT}.so
                /usr/${host}/lib/cmake/AppStreamQt  AppStreamQt-${SLOT}
            )
        fi
    else
        if option qt5 ; then
            arch_dependent_alternatives+=(
                /usr/${host}/include/AppStreamQt   AppStreamQt-${SLOT}
                /usr/${host}/lib/libAppStreamQt.so libAppStreamQt-${SLOT}.so
                /usr/${host}/lib/cmake/AppStreamQt AppStreamQt-${SLOT}
            )
        fi
    fi

    other_alternatives+=(
        /usr/share/gettext/its/metainfo.its metainfo-${SLOT}.its
        /usr/share/gettext/its/metainfo.loc metainfo-${SLOT}.loc
        /usr/share/installed-tests/${PN}    ${PN}-${SLOT}
        /usr/share/man/man1/appstreamcli.1  appstreamcli-${SLOT}.1
        /usr/share/metainfo/org.freedesktop.${PN}.cli.metainfo.xml
            org.freedesktop.${PN}.cli.metainfo-${SLOT}.xml
    )

    edo pushd "${IMAGE}"
    for file in usr/share/locale/*/LC_MESSAGES/${PN}.mo; do
        other_alternatives+=( /${file} ${PN}-${SLOT}.mo )
    done
    edo popd

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"

}

