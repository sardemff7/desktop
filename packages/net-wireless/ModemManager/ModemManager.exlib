# Copyright 2010 Joonas Sarajärvi <muepsj@gmail.com>
# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" user=mobile-broadband suffix=tar.bz2 new_download_scheme=true $(ever is_scm && echo branch=main) ]
require bash-completion
require meson
require gtk-icon-cache udev-rules vala [ vala_dep=true with_opt=true option_name=vapi ]
require systemd-service test-dbus-daemon
require option-renames [ renames=[ 'policykit polkit' 'vala vapi' ] ]

export_exlib_phases src_install

SUMMARY="Provides mobile 3G support for NetworkManager"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    mbim [[ description = [ Support for the Mobile Interface Broadband Model (MBIM) protocol ] ]]
    polkit
    qmi [[ description = [ Support for the Qualcomm MSM Interface (QMI) protocol ] ]]
    qrtr [[ description = [ Support for the Qualcomm QRTR protocol ] requires = [ qmi ] ]]
    systemd [[ description = [ Integrate with systemd-journald and systemd suspend/resume features ] ]]
    vapi [[ requires = [ gobject-introspection ] ]]
"

# tests want to connect to systemd journal
# and various other things (last checked: 1.4.12)
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.56]
        gnome-desktop/libgudev[>=232]
        sys-apps/dbus
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        mbim? ( net-libs/libmbim[>=1.30.0] )
        polkit? ( sys-auth/polkit:1[>=0.97] )
        qmi? ( net-libs/libqmi[>=1.34][mbim?][qrtr?] )
        qrtr? ( net-libs/libqrtr-glib[>=1.0.0] )
        systemd? ( sys-apps/systemd[>=209] )
    recommendation:
        net-misc/mobile-broadband-provider-info
"

MESON_SRC_CONFIGURE_PARAMS=(
    # Features here are the plugins
    # We enable all these that are available for our set of options
    --auto-features=auto
    -Ddbus_policy_dir=/usr/share/dbus-1/system.d
    -Dudev=true
    -Dudevdir=${UDEVDIR}
    -Dsystemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bash_completion'
    'gtk-doc gtk_doc'
    'gobject-introspection introspection'
    mbim
    qmi
    qrtr
    'systemd systemd_journal'
    'systemd systemd_suspend_resume'
    vapi
)
MESON_SRC_CONFIGURE_OPTIONS=(
    'polkit -Dpolkit=strict -Dpolkit=no'
)


ModemManager_src_install() {
    meson_src_install

    keepdir /etc/${PN}/connection.d
    keepdir /usr/$(exhost --target)/lib/${PN}/connection.d

    keepdir /etc/${PN}/fcc-unlock.d
    keepdir /usr/$(exhost --target)/lib/${PN}/fcc-unlock.d
}

