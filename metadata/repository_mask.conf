(
    app-crypt/keybase[~scm]
    app-text/zathura[~scm]
    compositor/sway[~scm]
    dev-libs/appstream[~scm]
    media/pipewire[~scm]
    media-sound/pasystray[~scm]
    net-apps/NetworkManager[~scm]
    net-apps/signon-ui[~scm]
    net-im/libquotient[~scm]
    net-im/telepathy-accounts-signon[~scm]
    net-libs/accounts-qt[~scm]
    net-libs/signon[~scm]
    net-misc/mobile-broadband-provider-info[~scm]
    net-wireless/ModemManager[~scm]
    sys-auth/elogind[~scm]
    sys-libs/libinput[~scm]
    sys-libs/wayland[~scm]
    sys-libs/wayland-protocols[~scm]
    sys-libs/wlroots[~scm]
    text-plugins/zathura-cb[~scm]
    text-plugins/zathura-djvu[~scm]
    text-plugins/zathura-pdf-mupdf[~scm]
    text-plugins/zathura-pdf-poppler[~scm]
    text-plugins/zathura-ps[~scm]
    x11-apps/alacritty[~scm]
    x11-apps/i3lock[~scm]
    x11-libs/girara[~scm]
    x11-libs/i3ipc-glib[~scm]
    x11-plugins/i3status[~scm]
    x11-wm/awesome[~scm]
    x11-wm/i3[~scm]
    wayland-apps/Waybar[~scm]
    wayland-apps/grim[~scm]
    wayland-apps/slurp[~scm]
    wayland-apps/swaylock[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm version ]
]]

net-www/chromium-stable[<126.0.6478.126] [[
    author = [ Tom Briden <tom@decompile.me.uk> ]
    date = [ 30 Jun 2024 ]
    token = security
    description = [ CVE-2024-629{0..3} ]
]]

dev-libs/nss[<3.98] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 17 Feb 2024 ]
    token = security
    description = [ CVE-2023-5388 ]
]]

net-im/telepathy-idle[<0.1.15] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 May 2013 ]
    token = security
    description = [ CVE-2007-6746 ]
]]

(
    net-im/telepathy-gabble[<0.16.6]
    net-im/telepathy-gabble[>0.17&<0.17.4]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 03 May 2013 ]
    *token = security
    *description = [ CVE-2013-1431 ]
]]

sys-auth/polkit:1[<0.120-r3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 19 Feb 2022 ]
    token = security
    description = [ CVE-2021-4115 ]
]]

dev-libs/nspr[<4.10.10] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 04 Nov 2015 ]
    token = security
    description = [ CVE-2015-7183 ]
]]

voip/mumble[<1.2.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 May 2014 ]
    token = security
    description = [ CVE-2014-375{5,6} ]
]]

sys-apps/udisks:2[<2.8.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Oct 2018 ]
    token = security
    description = [ CVE-2018-17336 ]
]]

x11-apps/xdg-utils[<1.1.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 May 2018 ]
    token = security
    description = [ CVE-2017-18266 ]
]]

net-apps/NetworkManager[<1.0.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Apr 2016 ]
    token = security
    description = [ CVE-2016-0764 ]
]]

net-www/firefox[<127.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 Jun 2024 ]
    token = security
    description = [ CVE-2024-{5687,5688,5689,5690,5691,5692,5693,5694,5695,
                              5696,5697,5698,5699,5700,5701} ]
]]

mail-client/thunderbird[<115.4.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 24 Oct 2023 ]
    token = security
    description = [ CVE-2023-{5721,5723,5724,5725,5728,5730} ]
]]

mail-libs/libetpan[<1.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 9 May 2017 ]
    token = security
    description = [ CVE-2017-8825 ]
]]

sys-apps/flatpak[<1.14.6] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 18 Apr 2024 ]
    token = security
    description = [ CVE-2024-32462 ]
]]

net-apps/NetworkManager[>=1.11&<1.12] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 16 May 2018 ]
    token = testing
    description = [ Mask unstable versions of NetworkManager ]
]]

net-dns/avahi[<0.8-r3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 May 2023 ]
    token = security
    description = [ CVE-2021-3468 ]
]]

mail-client/claws-mail[<4.0.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Sep 2021 ]
    token = security
    description = [ CVE-2021-37746 ]
]]

net-www/google-chrome-bin[<126.0.6478.126] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Jun 2024 ]
    token = security
    description = [ CVE-2024-62{90..93} ]
]]

net-libs/libproxy[<0.4.17] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Mar 2021 ]
    token = security
    description = [ CVE-2020-25219, CVE-2020-26154 ]
]]

dev-libs/spidermonkey[<102.8.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Feb 2023 ]
    token = security
    description = [ CVE-2023-25735 ]
]]

sys-auth/seatd[<0.6.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Mar 2022 ]
    token = security
    description = [ CVE-2022-25643 ]
]]

sys-libs/libinput[<1.20.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 20 Apr 2022 ]
    token = security
    description = [ CVE-2022-1215 ]
]]

sys-apps/flatpak-builder[<1.2.2] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 12 Nov 2022 ]
    token = security
    description = [ CVE-2022-21682 ]
]]

app-text/mupdf[<1.23.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Nov 2023 ]
    token = security
    description = [ CVE-2023-31794 ]
]]

sys-apps/flatpak[>=1.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 27 May 2024 ]
    token = pre-release
    description = [ Development version ]
]]

net-misc/nextcloudclient[<3.8.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Apr 2023 ]
    token = security
    description = [ CVE-2023-28999 ]
]]
